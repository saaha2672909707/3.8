public  class  Action{
    private double price;
    private String names;
    private int amount;

    public Action(String names ,double price,int amount){
        this.names = names;
        this.price = price;
        this.amount = amount;
    }


    public String getNames() {
        return names;
    }

    public void setNames (String names) {
        this.names = names;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Action{" +
                "price=" + price +
                ", names='" + names + '\'' +
                ", amount=" + amount +
                '}';
    }
}






