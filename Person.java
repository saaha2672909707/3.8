import java.util.List;
public  class Person {


    private String namePerson;
    private List<Action> favoriteActions;


    public Person(String namePerson, List<Action> favoriteActions ){
        this.namePerson = namePerson;
        this.favoriteActions = favoriteActions;
    }


    public String getNamePerson(){
        return namePerson;
    }

    public void  setNamePerson (String namePerson){
        this.namePerson = namePerson;
    }

    public List<Action> getFavoriteActions() {
        return favoriteActions;
    }

    public void setFavoriteActions(List<Action> favoriteActions) {
        this.favoriteActions = favoriteActions;
    }

    @Override
    public String toString() {
        return "Person{" +
                "namePerson='" + namePerson + '\'' +
                ", favoriteActions=" + favoriteActions +
                '}';
    }
}
