import java.time.LocalTime;
import java.util.List;

public  class Work extends Thread{
    private List<Action> actions;
    private List<Person> persons;

    public Work(List<Action> actions, List<Person> persons){
        this.actions = actions;
        this.persons = persons;
    }

    public void run() {

        for (int a = 0;a<120;a++){

            try {
                sleep(5000);

                for (Action actions : actions) {
                    for (Person person : persons) {
                        for (Action favoriteAction : person.getFavoriteActions()) {

                            if (actions.getNames().equals(favoriteAction.getNames()) && actions.getPrice() <= favoriteAction.getPrice()) {
                                actions.setAmount(actions.getAmount() - favoriteAction.getAmount());

                                System.out.println(LocalTime.now() +" "+ person.getNamePerson() + " bought action "
                                        + favoriteAction.getNames() + " remainder " + actions.getAmount());
                            }
                            if (actions.getNames().equals(favoriteAction.getNames()) && actions.getPrice() > favoriteAction.getPrice()){
                                System.out.println(LocalTime.now()+ " " + person.getNamePerson() + " action failed to buy "
                                        + favoriteAction.getNames());
                            }

                            if (actions.getAmount() <= 0){
                                break;
                            }


                        }
                    }

                }

        }catch (InterruptedException e) {

            }

        }
}
}
