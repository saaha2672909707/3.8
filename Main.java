import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {



        List<Action> actions = new ArrayList<>();
         Collections.addAll(actions,
                 new Action("Apple",100,3000),
                 new Action("Amazon",120,3500),
                 new Action("Asus",80,5000)

         );


         List<Person> personList = new ArrayList<>();



         List<Action> bobFavorite = new ArrayList<>();
         bobFavorite.add(new Action("Apple",100,10));
         bobFavorite.add(new Action("Amazon" , 115,5));

         List<Action> linaFavorite = new ArrayList<>();
         linaFavorite.add(new Action("Apple",99,7));
         linaFavorite.add(new Action("Asus",75,10));

         List<Action> tomFavorite = new ArrayList<>();
         tomFavorite.add(new Action("Amazon",110,6));
         tomFavorite.add(new Action("Asus" , 78,5));



         Collections.addAll(personList,
                 new Person("Bob" ,bobFavorite),
                 new Person("Lina",linaFavorite),
                 new Person("Tom",tomFavorite)
                 );

        ChangePrice changePrice = new ChangePrice(actions);

        Work work = new Work(actions,personList);
        work.start();
        changePrice.start();
    }
}
