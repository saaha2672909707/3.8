import java.time.LocalTime;
import java.util.List;
import java.util.Random;

public class ChangePrice extends Thread{

    public List<Action> actions;


    public ChangePrice(List<Action> actions){
         this.actions = actions;
    }

    public void run() {

        for (int a = 0;  a < 20 ;a++) {

            try {
                Thread.sleep(30000);



                for (Action action : actions) {
                    Random random = new Random();
                    int number = random.nextInt(2);
                    if (number == 0) {

                        action.setPrice(action.getPrice() + action.getPrice() * 0.03);
                        System.out.println( LocalTime.now() + " New price " + action.getNames() + " " + action.getPrice());
                    } else {
                        action.setPrice(action.getPrice() - action.getPrice() * 0.03);
                        System.out.println( LocalTime.now() + " New price " + action.getNames() + " " +action.getPrice());
                    }
                }
            }catch (InterruptedException e) {}
        }
    }
}
